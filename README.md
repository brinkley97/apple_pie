# Apple Pie

Special Topics: iOS Development Class at Claflin University

## Features
- Spell the word by guessing letters
- Track wins and losses

---
## Contributors
* Detravious Brinkley <d.brinkley97@gmail.com>

---
## Run Code
1. Clone Repo
2. Open xcode

---
## Resources
App Development With Swift book in app store




