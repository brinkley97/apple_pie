//
//  ViewController.swift
//  applePie
//
//  Created by Detravious Brinkley on 2/15/20.
//  Copyright © 2020 Detravious Brinkley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//List of words
    var listOfWords = ["biceps", "legs", "triceps", "forearm", "calf", "traps", "chest"]
    let incorrectMovesAllowed = 7
    
    //new round begins after a win or loss
    var totalWins = 0 {
        didSet {
            newRound()
        }
    }
    //new round begins after a win or loss
    var totalLosses = 0 {
        didSet {
            newRound()
        }
    }
    
    var currentGame: Game!
    
//Start of outlets and actions
    @IBOutlet var treeImageView: UIImageView!
    @IBOutlet var correctWordLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    
    //connection of letters
    @IBOutlet var letterButtons: [UIButton]!
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        sender.isEnabled = false
        let letterString = sender.title(for: .normal)!
        let letter = Character(letterString.lowercased())
        currentGame.playerGuessed(letter: letter)
        updateGameState()
        
    }
//End of outlets and actions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        newRound()
    }
    
    //Each round will start with a new word and will reset incorrectMovesAllowed
    func newRound(){
        if !listOfWords.isEmpty{
            let newWord = listOfWords.removeFirst()
            currentGame = Game(word: newWord, incorrectMovesRemaining: incorrectMovesAllowed, guessedLetters: [])
            enableLetterButtons(true)
            updateUI()
        } else {
            enableLetterButtons(false)
        }
    }
    
    func updateUI(){
        
        //Storing each of the newly-created strings
        var letters = [String]()
        
        //Converting array of characters in formattedWord into an array of strings
        for letter in currentGame.formattedWord{
            letters.append(String(letter))
        }
        
        let wordWithSpacing = letters.joined(separator: " ")
        correctWordLabel.text = wordWithSpacing
        scoreLabel.text = "Wins: \(totalWins), Losses: \(totalLosses)"
        treeImageView.image = UIImage(named: "Tree \(currentGame.incorrectMovesRemaining)")
    }
    
    //if win, then add to win total; same for loss
    func updateGameState(){
        if currentGame.incorrectMovesRemaining == 0 {
            totalLosses += 1
        } else if currentGame.word == currentGame.formattedWord {
            totalWins += 1
        } else{
            updateUI()
        }
    }

    //Enable buttons
    func enableLetterButtons(_ enable: Bool) {
        for button in letterButtons {
            button.isEnabled = enable
        }
    }
}

