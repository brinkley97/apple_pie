//
//  Game.swift
//  applePie
//
//  Created by Detravious Brinkley on 2/17/20.
//  Copyright © 2020 Detravious Brinkley. All rights reserved.
//

import Foundation

struct Game {
    var word: String
    var incorrectMovesRemaining: Int
    var guessedLetters: [Character]

//update when a letter is guessed
    mutating func playerGuessed(letter: Character){
        guessedLetters.append(letter)
        
        if !word.contains(letter){
            incorrectMovesRemaining -= 1
        }
    }
    
    var formattedWord: String {
        var guessedWord = ""
        for letter in word{
            if guessedLetters.contains(letter){
                guessedWord += "\(letter)"
            }
            else {
                guessedWord += "_"
            }
        }
        return guessedWord
    }
    
}
